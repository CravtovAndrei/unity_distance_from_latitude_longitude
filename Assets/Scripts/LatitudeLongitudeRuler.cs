﻿using System;
using UnityEngine;

public class LatitudeLongitudeRuler
{
    public enum EarthRadiusUnits
    {
       Kilometers,
       Miles
    }
    
    public EarthRadiusUnits _earthRadiusUnits
    {
        get => _earthRadiusUnits = EarthRadiusUnits.Kilometers;
        set => value = EarthRadiusUnits.Kilometers;
    }
    public float GetStraightDistanceFromLatLonInKilometers(float lat1, float lon1, float lat2, float lon2)
    {
        float Radius = SwitchUnits();

        float dLat = Mathf.Deg2Rad * (lat2 - lat1);
        float dLon = Mathf.Deg2Rad * (lon2 - lon1);

        double a =
            Mathf.Sin(dLat / 2) * Mathf.Sin(dLat / 2) +
            Mathf.Cos(Mathf.Deg2Rad * lat1) * Math.Cos(Mathf.Deg2Rad * lat2) *
            Mathf.Sin(dLon / 2) * Mathf.Sin(dLon / 2);


        var c = 2 * Mathf.Atan2(Mathf.Sqrt((float) a), Mathf.Sqrt(1 - (float) a));
        var d = Radius * c; 
        return d;
    }


    float SwitchUnits()
    {
        switch (_earthRadiusUnits)
        {
            case EarthRadiusUnits.Kilometers:
                return 6371f;
            case EarthRadiusUnits.Miles:
                return 3958.5f;
            default:
                return 6371f;
        }
    }
    
    
}