﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DistanceChecker : MonoBehaviour
{
    [SerializeField] TMP_InputField _lat1Input;
    [SerializeField] TMP_InputField _lot1Input;
    [SerializeField] TMP_InputField _lat2Input;
    [SerializeField] TMP_InputField _lot2Input;
    [SerializeField] TextMeshProUGUI _distanceText;

    float _latitude1;
    float _longitude1;
    float _latitude2;
    float _longitude2;

    readonly LatitudeLongitudeRuler _ruler = new LatitudeLongitudeRuler();

    void GetLatLonFromInput()
    {
        bool result = float.TryParse(_lat1Input.text, out _latitude1) &&
                      float.TryParse(_lot1Input.text, out _longitude1) &&
                      float.TryParse(_lat2Input.text, out _latitude2) &&
                      float.TryParse(_lot2Input.text, out _longitude2);
        
        if (!result) return;

        List<TMP_InputField> _allInputs = new List<TMP_InputField>();
        _allInputs.AddRange(new TMP_InputField[] {_lat1Input, _lot1Input, _lat2Input, _lot2Input});

        List<string> allInputText = new List<string>();
        allInputText.AddRange(new string[] {_lat1Input.text, _lot1Input.text, _lat2Input.text, _lot2Input.text});
        ReplaceAllDots(allInputText, _allInputs);


        if (result)
        {
            _latitude1 = float.Parse(_lat1Input.text);
            _longitude1 = float.Parse(_lot1Input.text);
            _latitude2 = float.Parse(_lat2Input.text);
            _longitude2 = float.Parse(_lot2Input.text);

            SetDistance();
        }
    }

    void ReplaceAllDots(List<string> listOfStrings, List<TMP_InputField> listOfInputs)
    {
        for (var i = 0; i < listOfStrings.Count; i++)
        {
            listOfInputs[i].text = listOfStrings[i].Replace(".", ",");
        }
    }

    void SetDistance()
    {
        _distanceText.text = _ruler
            .GetStraightDistanceFromLatLonInKilometers(_latitude1, _longitude1, _latitude2, _longitude2).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return)) GetLatLonFromInput();
    }
}